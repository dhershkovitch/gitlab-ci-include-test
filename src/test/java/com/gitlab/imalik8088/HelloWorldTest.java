package com.gitlab.imalik8088;

import org.junit.Assert;
import org.junit.Test;

public class HelloWorldTest {

    @Test
    public void testHelloMessage() {
        String[] testArgs = new String[]{"GitLab", "CI", "Include", "Tag"};
        String helloMessage = HelloWorld.helloMessage(testArgs);
        Assert.assertEquals(helloMessage, "Hello GitLab-CI-Include-Tag");
    }
}